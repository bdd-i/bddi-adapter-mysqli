<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI\LowLevel;

use Luri\BddI\Common\{Db, SqlRequest, SqlResponse};

class LlMysqli implements Db {
	/**
	 * Instance of Mysqli classe
	 * @var \Mysqli
	 */
	protected $dbInstance = null;

	/**
	 * Array with corespondance between dbId and dbName
	 * key : dbId
	 * value : dbName
	 *
	 * @var array
	 */
	protected $dbname = [];

	/**
	 * Id of active database
	 * @var string
	 */
	protected $dbactive = "";



	public function __construct($server, $login, $password, $charset) {
		//Active Mysqli exception... yep, php help is clear :D
		mysqli_report(MYSQLI_REPORT_ALL & ~MYSQLI_REPORT_INDEX);

		//connect
		$this->dbInstance = new \mysqli($server, $login, $password);

		//Change charset (function mysqli::set_charset not return an exception if it fail.)
		if (! $this->dbInstance->set_charset($charset)) {
			throw new \mysqli_sql_exception("Not a good charset");
		}
	}

	public function changeDb($dbId, $dbName = "") {
		if ($this->dbactive != $dbId) {
			//Active database is different than database requested, change.
			if (!empty($dbName)) {
				//Save this dbname
				$this->dbname[$dbId] = $dbName;

			} else {
				//No DbName, Search dnname
				if (empty($this->dbname[$dbId])) {
					//Not found
					throw new \BadFunctionCallException("Error : dbName not found for id : $dbId");
				}
				$dbName = $this->dbname[$dbId];
			}

			//Change database
			$this->dbInstance->select_db($dbName);

			//And save active databse
			$this->dbactive = $dbId;
		}
	}

	public function exe(SqlRequest $sqlrequest) : SqlResponse {
		$mysqlResult = null;

		if (! $sqlrequest->isData()) {
			//No Data, Simple request
			$this->dbInstance->real_query($sqlrequest);
			if($this->dbInstance->field_count) {
				//Use field_count to determine if the query should have produced a non-empty result set or not without knowing the nature of the query.
				//If we are here, query have a result (this result can be empty)
				$mysqlResult = $this->dbInstance->store_result();
			} else {
				$insertid = $this->dbInstance->insert_id;
				$affectedrows = $this->dbInstance->affected_rows;
			}

		} else {
			//We have Data, need prepared statement
			//By default,  SqlRequest return anonymous Parameters (?)
			$param_bp = [];
			$param_bp[0] = ''; //Type of data
			$datas = [];
			foreach ($sqlrequest->getDatas() as $k=>$v) {
				//Type de données
				if (is_bool($v)) {
					//Boolean not suuported
					//Must be convert in SqlRequest class
					throw new \UnexpectedValueException("Bool is not supported. Normally, your SqlRequest class must be convert that.");
				} elseif (is_float($v)) {
					$param_bp[0] .= 'd';
				} elseif (is_int($v)) {
					$param_bp[0] .= 'i';
				} else {
					$param_bp[0] .= 's';
				}

				//Save value
				$datas[$k] = $v; //because generator not work with &$gen syntax
				$param_bp[] = &$datas[$k];
			}


			//Prepare and Execute Request
			$statement = $this->dbInstance->prepare($sqlrequest);

			// ...$parambp unpack array $param_bp into the argument list
			// see : https://secure.php.net/manual/en/functions.arguments.php
			@$test = $statement->bind_param(...$param_bp);
			if ($test === false) {
				//on s'en fout, c'est reporté ensuite en erreur d'exécution
				//ici, c'est juste pour supprimer le warning php pour les test de la version beta
				//et comme ça c'est préparé pour les futurs execeptions bddi
			}

			$statement->execute();
			if ($statement->field_count) {
				//Use field_count to determine if the query should have produced a non-empty result set or not without knowing the nature of the query.
				//If we are here, query have a result (this result can be empty)
				$mysqlResult = $statement->get_result();
			} else {
				$insertid = $statement->insert_id;
				$affectedrows = $statement->affected_rows;
			}
			$statement->close();
		}

		//Return appropriate SqlResponse
		if ($mysqlResult) {
			//We have a response
			return new LlMysqliResponseSelect($mysqlResult);
		} else {
			//No response, probably a INSERT or UPDATE query
			return new LlMysqliResponseInsert($insertid, $affectedrows);
		}
	}

	public function transactionStart() {
		$this->dbInstance->autocommit(false);
		$this->dbInstance->begin_transaction();
	}

	public function transactionCommit() {
		$this->dbInstance->commit();
		$this->dbInstance->autocommit(true);
	}

	public function transactionRollback() {
		$this->dbInstance->rollback();
		$this->dbInstance->autocommit(true);
	}


}
?>