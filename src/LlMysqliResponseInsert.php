<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI\LowLevel;

use Luri\BddI\Common\SqlResponse;

/**
 * Represent an no SELECT, SHOW, DESCRIBE or EXPLAIN reponse
 *
 * This return :
 * For a SQL INSERT or UPDATE, this return auto generated id used in the latest query. If the modified
 * table does not have a column with the AUTO_INCREMENT attribute, this function will return zero.
 *
 * For other SQL query, this return equivalent of empty array
 *
 * Count : Return the number of affected rows.
 *
 */
class LlMysqliResponseInsert implements SqlResponse {

	protected $value = "";

	protected $nb = 0;

	protected $i = 0;

	public function __construct($value = "", $nb = 0) {
		$this->value = $value;
		$this->nb = $nb;
	}


	public function setColumns($columns = SqlResponse::ALLCOLUMNS) {
	}

	public function setReturnIndex($indextype = SqlResponse::INDEXSTRING) {
	}

	public function getLine($col, $value): array {
		return [$this->value];
	}


	public function count() {
		return $this->nb;
	}


	public function current() {
		return $this->value;
	}

	public function key() {
		return 0;
	}

	public function next() {
		++$this->i;
	}

	public function rewind() {
		$this->i = 0;
	}

	public function valid() {
		return ($this->i == 0 AND $this->i !== "");
	}



	public function offsetExists($offset) {
		return ($this->i !== "" AND $offset == 0);
	}

	public function offsetGet($offset) {
		if ($this->i !== "" AND $offset == 0) {
			return $this->value;
		} else {
			throw new \OutOfBoundsException("This key is not valid");
		}
	}

	public function offsetSet($offset, $value) {
		throw new \BadMethodCallException("This operation is not supported");
	}

	public function offsetUnset($offset) {
		throw new \BadMethodCallException("This operation is not supported");
	}

}
?>