<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI\LowLevel;

use Luri\BddI\Common\{NotFoundException, SqlResponse};

class LlMysqliResponseSelect implements SqlResponse {

	/**
	 * The result
	 *
	 * @var \mysqli_result
	 */
	protected $response = null;

	/**
	 * Column to be returned
	 *
	 * @var string
	 */
	protected $columnReturn = SqlResponse::ALLCOLUMNS;

	/**
	 * Type of index of result (int, string or both)
	 * @var int
	 */
	protected $returnIndex = MYSQLI_ASSOC;

	/**
	 * Name of fields of the result
	 *
	 * @var array
	 */
	protected $columnsName = [];

	/**
	 * Position in the result
	 * @var int
	 */
	protected $internalkey = 0;







	public function __construct(\mysqli_result $result) {
		//Store result
		if (! ($result instanceof \mysqli_result)) {
			throw new \InvalidArgumentException("result not have the good type");
		}

		$this->response = $result;

		//Save field name and position
		$this->storeColumns();
	}

	/**
	 * Destructeur, libère la mémoire
	 */
	public function __destruct() {
		$this->response->free();
	}


	/**
	 * Search and Store all column name of result
	 *
	 */
	protected function storeColumns() {
		//Search and Store all info
		for ($i=0; $i < $this->response->field_count; ++$i) {
			$info = $this->response->fetch_field_direct($i);
			$this->columnsName[$i] = $info->name;
		}
	}

	/**
	 * Return only wanted column of a line of result
	 *
	 * @param array $line A line of result. Must contain index and/or field name accorging to $this->returnIndex parameter
	 * @return type
	 */
	protected function filter($line) {
		if ($this->columnReturn == SqlResponse::ALLCOLUMNS) {
			//Want all column
			return $line;
		}

		//Want filter
		$ret= [];
		foreach ($this->columnReturn as $colwanted) {
			if (is_int($colwanted)) {
				//index of field wanted
				switch ($this->returnIndex) {
					case MYSQLI_NUM:
						//want only numeric key
						$ret[$colwanted] = $line[$colwanted];
						break;

					case MYSQLI_ASSOC:
						//want only string key
						//Retreive field name
						$key = $this->columnsName[$colwanted];
						if (!array_key_exists($key, $ret)) {
							$ret[$key] = $line[$key];
						}
						break;

					case MYSQLI_BOTH:
						//want both int and string keys
						//numeric
						$ret[$colwanted] = $line[$colwanted];
						//Field name
						$key = $this->columnsName[$colwanted];
						if (!array_key_exists($key, $ret)) {
							$ret[$key] = $line[$key];
						}
						break;
				}

			} else {
				//name of field wanted
				switch ($this->returnIndex) {
					case MYSQLI_NUM:
						//want only numeric key
						//Retreive index of field
						$key = array_search($key, $this->columnsName);
						$ret[$key] = $line[$key];
						break;

					case MYSQLI_ASSOC:
						//want only string key
						$ret[$colwanted] = $line[$colwanted];
						break;

					case MYSQLI_BOTH:
						//want coth int and string keys
						//numeric
						$key = array_search($key, $this->columnsName);
						$ret[$key] = $line[$key];
						//fieldname
						$ret[$colwanted] = $line[$colwanted];
						break;
				}
			}
		}

		return $ret;
	}




	public function setColumns($columns = SqlResponse::ALLCOLUMNS) {
		//test
		if (!is_array($columns) AND $columns != SqlResponse::ALLCOLUMNS) {
			//Invalid data
			throw new \InvalidArgumentException("Parameter columns ($columns) is not valid");
		}

		if (is_array($columns)) {
			//Verify of all column exist in result
			foreach ($columns as $v) {
				if (is_int($v)) {
					if (! array_key_exists($v, $this->columnsName)) {
						throw new \OutOfBoundsException("Field $v not exist in result");
					}

				} else {
					if (array_search($v, $this->columnsName)===false) {
						throw new \OutOfBoundsException("Field $v not exist in result");
					}
				}
			}
		}

		//Valid Values, we store
		$this->columnReturn = $columns;
	}

	public function setReturnIndex($indextype = SqlResponse::INDEXSTRING) {
		switch ($indextype) {
			case SqlResponse::INDEXINT:
				$this->returnIndex = MYSQLI_NUM;
				break;

			case SqlResponse::INDEXSTRING:
				$this->returnIndex = MYSQLI_ASSOC;
				break;

			case SqlResponse::INDEXBOTH:
				$this->returnIndex = MYSQLI_BOTH;
				break;

			default :
				//Error
				throw new \InvalidArgumentException("undefined indextype $indextype");
		}
	}

	public function getLine($col, $value): array {
		//Verify if $col exist
		$finfo = $this->response->fetch_fields();
		if (is_int($col)) {
			//numerical index of column
			if (!array_key_exists($col, $finfo)) {
				throw new \OutOfBoundsException("Field $col not exist in result");
			}
		} else {
			//name of column
			$find = false;
			foreach ($finfo as $val) {
				if ($val->name == $col) {
					$find = true;
					break;
				}
			}
			if (! $find) {
				throw new \OutOfBoundsException("Field $col not exist in result");
			}
		}


		//Reset
		$this->response->data_seek(0);

		//Browse result to find 1rst wanted line
		//Take both numerical and fieldname index beacause I want this to work with all king of $col
		while($line = $this->response->fetch_array(MYSQLI_BOTH)) {
			if ($line[$col]==$value) {
				//We find !
				$find = $this->filter($line);

				//We must return only asked index
				if ($this->returnIndex == MYSQLI_NUM) {
					//We must return only numerical index
					$find = array_filter($find, function($k) {
						return is_int($k);
					}, ARRAY_FILTER_USE_KEY);

				} else if ($this->returnIndex == MYSQLI_ASSOC) {
					//We must return only associative index
					$find = array_filter($find, function($k) {
						return !is_int($k);
					}, ARRAY_FILTER_USE_KEY);
				}

				//return result
				return $find;
			}
		}

		//If we arrivc here, this means we not found the result
		throw new NotFoundException("We Don't find $value in columns $col");
	}


	public function count() {
		return $this->response->num_rows;
	}


	public function current() {
		$t = $this->filter($this->response->fetch_array($this->returnIndex));

		$this->response->data_seek($this->internalkey);

		return $t;
	}

	public function key() {
		return $this->internalkey;
	}

	public function next() {
		++$this->internalkey;
		$this->response->data_seek($this->internalkey);
	}

	public function rewind() {
		$this->response->data_seek(0);
	}

	public function valid() {
		return ($this->internalkey < $this->response->num_rows);
	}



	public function offsetExists($offset) {
		return ($offset >= 0 AND $offset < $this->response->num_rows);
	}

	public function offsetGet($offset) {
		$this->response->data_seek($offset);

		$t = $this->filter($this->response->fetch_array($this->returnIndex));

		$this->response->data_seek($this->internalkey);

		return $t;
	}

	public function offsetSet($offset, $value) {
		throw new \BadMethodCallException("This operation is not supported");
	}

	public function offsetUnset($offset) {
		throw new \BadMethodCallException("This operation is not supported");
	}

}
?>