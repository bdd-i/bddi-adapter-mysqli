<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

define('TEST_MYSQL_SERVER', '127.0.0.1');
define('TEST_MYSQL_LOGIN', 'root');
define('TEST_MYSQL_PW', '');
define('TEST_MYSQL_PW_ERROR', '12345');
define('TEST_MYSQL_CHARSET_ERROR', '12345');
define('TEST_MYSQL_DB1', 'test');
define('TEST_MYSQL_DB2', 'test2');
define('TEST_MYSQL_DB_NO_ACCESS', 'te'); //This database must not existe or user not have access to this db.
?>