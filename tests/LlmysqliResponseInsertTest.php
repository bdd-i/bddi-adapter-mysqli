<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

use PHPUnit\Framework\TestCase;
use Luri\BddI\LowLevel\{LlMysqliResponseInsert, LlMysqli};
use Luri\BddI\Common\SqlResponse;


class LlMysqlIResponseInsertTest extends TestCase {

	/**
	 *
	 * @var LlMysqliResponseInsert
	 */
	protected $response;

	/**
	 * Before each test
	 */
	protected function setUp() {
		//last_insert_id, nombre d'enregistrment modifié
		$this->response = new LlMysqliResponseInsert(1, 5);
	}

	/**
	 * After each test
	 */
	public function tearDown() {
		$this->response = null;
	}

	public function testForeach() {

		foreach ($this->response as $k=>$v) {
			$this->assertEquals(0, $k);
			$this->assertEquals(1, $v); //last_insert_id
		}
	}

	public function testArrayAccess() {
		//[0] must return last_insert_id
		$this->assertEquals(1, $this->response[0]);
		//Other must return	an exception
		$this->expectException("OutOfBoundsException");
		$this->response[1];
	}


	public function testCount() {
		$this->assertEquals(5, count($this->response));
	}

	public function testGetLine() {
		//must return last_insert_id
		$this->assertEquals([1], $this->response->getLine(0, 0));
		$this->assertEquals([1], $this->response->getLine(30, 2));
	}

}
?>