<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

use PHPUnit\Framework\TestCase;
use Luri\BddI\LowLevel\LlMysqli;
use Luri\BddI\Common\{ SqlRequest, SqlResponse};

require('Config.php');

class LlMysqliTest extends TestCase {
	/**
	 *
	 * @var ReflectionProperty
	 */
	protected static $property;


	public static  function setUpBeforeClass() {
		parent::setUpBeforeClass();

		//Reflexion helper to access $dbactive propriety
		self::$property = self::getPrivateProperty('\Luri\BddI\LowLevel\LlMysqli', 'dbactive' );

		//Create SQL DATA
		$db = new mysqli(TEST_MYSQL_SERVER, TEST_MYSQL_LOGIN, TEST_MYSQL_PW);
		$db->set_charset('utf8');
		$db->select_db(TEST_MYSQL_DB1);

		//create table singer
		$db->real_query('CREATE TABLE `singer` ('
			. '`idsinger` INT UNSIGNED NOT NULL AUTO_INCREMENT , '
			. '`name` VARCHAR(100) NOT NULL , '
			. '`nationality` VARCHAR(100) NOT NULL , '
			. 'PRIMARY KEY (`idsinger`)) ENGINE = MyISAM;');
	}

	/**
	 * Delete SQL dataused for test
	 */
	public static function tearDownAfterClass() {
		$db = new mysqli(TEST_MYSQL_SERVER, TEST_MYSQL_LOGIN, TEST_MYSQL_PW);
		$db->select_db(TEST_MYSQL_DB1);
		$db->real_query('DROP TABLE `singer`;');
	}

	/**
 	 * getPrivateProperty
 	 *
 	 * @author	Joe Sexton <joe@webtipblog.com>
 	 * @param 	string $className
 	 * @param 	string $propertyName
 	 * @return	ReflectionProperty
 	 */
	public static function getPrivateProperty( $className, $propertyName ) {
		$reflector = new ReflectionClass( $className );
		$property = $reflector->getProperty( $propertyName );
		$property->setAccessible( true );

		return $property;
	}


	public function testConstructWithFalseParameters() {
		$this->expectException(\mysqli_sql_exception::class);
		$db = new LlMysqli(TEST_MYSQL_SERVER, TEST_MYSQL_LOGIN, TEST_MYSQL_PW_ERROR, 'utf8');
		$this->assertNotInstanceOf(LlMysqli::class, $db);
	}

	public function testConstructWithFalseCharset() {
		$this->expectException(\mysqli_sql_exception::class);
		$db = new LlMysqli(TEST_MYSQL_SERVER, TEST_MYSQL_LOGIN, TEST_MYSQL_PW, TEST_MYSQL_CHARSET_ERROR);
		$this->assertNotInstanceOf(LlMysqli::class, $db);
	}

	/**
	 *
	 * @return LlMysqli
	 */
	protected function createSQLConnexion() {
		$db = new LlMysqli(TEST_MYSQL_SERVER, TEST_MYSQL_LOGIN, TEST_MYSQL_PW, 'utf8');
		$db->changeDb('TEST', TEST_MYSQL_DB1);
		return $db;
	}

	/**
	 *
	 * @return LlMysqli
	 */
	public function testConstruct() {
		$db = new LlMysqli(TEST_MYSQL_SERVER, TEST_MYSQL_LOGIN, TEST_MYSQL_PW, 'utf8');
		$this->assertInstanceOf(LlMysqli::class, $db);

		return $db;
	}

	/**
	 * @depends testConstruct
	 */
	public function testInitialChangeDb(LlMysqli $db) {
		//Change database (correct )
		$db->changeDb('DBID1', TEST_MYSQL_DB1);
		$this->assertEquals('DBID1', self::$property->getValue($db));

		return $db;
	}

	/**
	 * @depends testInitialChangeDb
	 */
	public function testChangeDbButDbNameUnknownMustThrownException(LlMysqli $db) {
		$this->expectException(\BadFunctionCallException::class);
		//Change database (incorrect : no database name registered)
		$db->changeDb('DBID2');

		return $db;
	}

	/**
	 * @depends testInitialChangeDb
	 */
	public function testChangeDbToDb2(LlMysqli $db) {
		//Change database (correct)
		$db->changeDb('DBID2', TEST_MYSQL_DB2);
		$this->assertEquals('DBID2', self::$property->getValue($db));

		return $db;
	}

	/**
	 * @depends testChangeDbToDb2
	 */
	public function testChangeDbToDb1WithOnlyDbId(LlMysqli $db) {
		//Change database (correct)
		$db->changeDb('DBID1');
		$this->assertEquals('DBID1', self::$property->getValue($db));
	}

	public function testTransactionStart() {
		$this->expectNotToPerformAssertions();

		$db = $this->createSQLConnexion();
		$db->transactionStart();
	}


	public function testTransactionCommit() {
		$this->expectNotToPerformAssertions();

		$db = $this->createSQLConnexion();
		$db->transactionStart();
		$db->transactionCommit();
	}

	public function testTransactionRollback() {
		$this->expectNotToPerformAssertions();

		$db = $this->createSQLConnexion();
		$db->transactionStart();
		$db->transactionRollback();
	}

	public function testExecuteWithErrorInSqlRequest() {
		$this->expectException(\mysqli_sql_exception::class);

		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn('SELECT * FROM sin WHERE id=2');
		$request->method('isData')->willReturn(false);

		//Test
		$db = $this->createSQLConnexion();
		$response = $db->exe($request);

	}

	public function testExecuteWithCorrectSqlRequestInsert() {
		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn("INSERT INTO singer (idsinger, name, nationality) VALUES ('5', 'Ronan Parke', 'British')");
		$request->method('isData')->willReturn(false);

		//Test
		$db = $this->createSQLConnexion();
		$res = $db->exe($request);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMysqliResponseInsert::class, $res);
		$this->assertSame(1, count($res), 'Incorrect number of affected row');

		//
		//Other Test
		//
		//Création du bouchon (Mock) simulant une vrai requète
		$request2 = $this->createMock(SqlRequest::class);
		$request2->method('__tostring')->willReturn("INSERT INTO singer (name, nationality) VALUES ('Greyson Chance', 'U.S.A.'), ('Bars And Melody', 'British')");
		$request2->method('isData')->willReturn(false);

		//Test
		$res = $db->exe($request2);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMysqliResponseInsert::class, $res);
		//Nb of affected Row
		$affected = count($res);
		$this->assertSame(2, $affected , "Incorrect number of affected row: $affected");
		//LAst insert id : id inserted : 6 and 7. Last insert id return the first id autogenerated (6 here)
		$this->assertSame(6, $res[0], "Incorrest last insert id : {$res[0]}");

	}

	public function testExecuteWithCorrectSqlRequestUpdate() {
		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn("UPDATE singer SET nationality = 'French' WHERE idsinger = 5");
		$request->method('isData')->willReturn(false);

		//Test
		$db = $this->createSQLConnexion();
		$res = $db->exe($request);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMysqliResponseInsert::class, $res);
		$this->assertSame(1, count($res), 'Incorrect number of affected row');

		//
		// Other test With Data Parameters
		//
		$request2 = $this->createMock(SqlRequest::class);
		$request2->method('__tostring')->willReturn("UPDATE singer SET nationality = 'Brit.' WHERE idsinger = ?");
		$request2->method('isData')->willReturn(true);
		$request2->method('getDatas')->willReturn([5]);
		
		$res = $db->exe($request2);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMysqliResponseInsert::class, $res);
		//Nb of affected Row
		$affected = count($res);
		$this->assertSame(1, $affected , "Incorrect number of affected row: $affected");
	}

	/**
	 * @depends testExecuteWithCorrectSqlRequestInsert
	 */
	public function testExecuteWithCorrectSqlRequestSelect() {
		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn("SELECT * FROM singer");
		$request->method('isData')->willReturn(false);

		//Test
		$db = $this->createSQLConnexion();
		$res = $db->exe($request);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMysqliResponseSelect::class, $res);
	}

	/**
	 * @depends testExecuteWithCorrectSqlRequestInsert
	 */
	public function testExecuteWithParameterMarkersAndHaveAResult() {
		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn("SELECT * FROM singer WHERE idsinger=?");
		$request->method('isData')->willReturn(true);
		$request->method('getDatas')->willReturn([5]);

		//Test
		$db = $this->createSQLConnexion();
		$res = $db->exe($request);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMysqliResponseSelect::class, $res);
	}

	/**
	 * @depends testExecuteWithCorrectSqlRequestInsert
	 */
	public function testExecuteWithParameterMarkersAndHaveEmptyResult() {
		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn("SELECT * FROM singer WHERE idsinger=?");
		$request->method('isData')->willReturn(true);
		$request->method('getDatas')->willReturn(new ArrayObject([1]));

		//Test
		$db = $this->createSQLConnexion();
		$res = $db->exe($request);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMysqliResponseSelect::class, $res);
	}

	/**
	 * @depends testExecuteWithCorrectSqlRequestInsert
	 */
	public function testExecuteWithBadNumberOfParameterMarkers() {
		$this->expectException(\mysqli_sql_exception::class);

		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn("SELECT * FROM singer WHERE idsinger=? OR name LIKE ? ");
		$request->method('isData')->willReturn(true);
		$request->method('getDatas')->willReturn(new ArrayObject([5]));

		//Test
		$db = $this->createSQLConnexion();
		$res = $db->exe($request);
	}

	/**
	 * @depends testExecuteWithCorrectSqlRequestInsert
	 */
	public function testExecuteWithBoolParameter() {
		$this->expectException('\UnexpectedValueException');

		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn("SELECT * FROM singer WHERE idsinger=?");
		$request->method('isData')->willReturn(true);
		$request->method('getDatas')->willReturn(new ArrayObject([true]));

		//Test
		$db = $this->createSQLConnexion();
		$res = $db->exe($request);
	}
}
?>